//WAP to find the sum of n complex numbers using structures and 4 or more functions.
#include<stdio.h>
typedef struct
{
char name[100];
int scored_works_number;
int scored_works[100];
float final_score;
char final_grade;

}student;
typedef struct
{
char course[100];
int weights_number;
float weights[100];
int students_number;
student students[100];

}gradebook;

student input_student(int scored_works_number)
{
student s;
printf(“Enter the name of the student\n”);
scanf(“%s”,s.name);

s.scored_works_number=scored_works_number;
printf(“Enter the scores\n”);
for(int i=0;i<s.scored_works_number;i++)
{
scanf(“%d”,&s.scored_works[i]);
}
return s;
}
gradebook input_grade_book() 
{
gradebook g;
printf(“Enter name of the course\n”);
scanf(“%s”,g.course);
printf(“Enter number of students\n”);
scanf(“%d”,&g.students_number);

printf(“Enter number of weights\n”);
scanf(“%d”,&g.weights_number);

printf(“Enter weights\n”);
for(int i=0;i<g.weights_number;i++)
{
scanf(“%f”,&g.weights[i]);
}
for(int i=0;i<g.students_number;i++)
{
g.students[i]=input_student(g.weights_number);
}
return g;
}
void input_n_gradebook(int n,gradebook g[n])
{
for(int i=0;i<n;i++)
{
g[i]=input_grade_book();
}
}

float sumof_weights(float weights[],int n_weight)
{
float sum=0.0;
for(int i=0;i<n_weight;i++)
{
sum+=weights[i];
}
}

char score_to_grade(float score)
{
if(score<60)
return ‘F’;
if(score<70)
return ‘D’;
if(score<80)
return ‘C’;
if(score<900)
return ‘B’;
if(score<100)
return ‘A’;
}

void
compute_grade_one_student(student*ss,gradebook *gg)
{
ss->final_score+=ss->scored_works[i]*gg->weights[i];
}

float w = sumof_weights(gg->weights;gg->weights_number);
ss->final_score /= w;
ss->final_grade=score_to_grade(ss->final_score);
}

void
compute_grade_allstudent(gradebook *gg)
{
for(int i=0;i<gg->students_number;i++)
{
compute_grade_one_student(&gg->students[i],gg);
}
}

void
compute_grade_allgradebooks(int n,gradebook g[n])
{
for(int i=0;i<n;i++)
{
compute_grade_allstudent(&g[i]);
}
}

void print_grade_one_student(student s1)
{
printf(“%s\t%0.2f %c\n”,s1.names,s1.final_score,s1.final_grade);
}
void
 print_grade_n_student(gradebook g1)
{

for(int i=0;i<g1.students_number;i++)
{
print_grade_one_student(g1.students[i]);
}

}

void print(int n,gradebook g[])
{
for(int i=0;i<n;i++)
{
printf(“%s\n”,g[i].course);
print_grade_n_student(g[i]);
printf(“\n”);
}
}

int main()
{
int n;
printf(“Enter the number of gradebooks:”);
scanf(“%d”,&n);
gradebook g[n];
while(n){
input_n_gradebook(n,g);
break;
}
while(n){
compute_grade_allgradebooks(n,g);
break;

}
while(n)
{
print(n,g);
break;
}



return 0;

}

